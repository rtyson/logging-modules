package com.veridiumid.sdk.debugging;

import android.graphics.Bitmap;
import android.os.Environment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.veridiumid.sdk.imaging.model.ImageHolder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by josiah on 21/07/2016.
 *
 * Saves all photos taken (not all frames streamed) for debugging purposes.
 * Logs metadata about the circumstances of taking that photo in a csv file.
 *
 * This is purely for debugging purposes.
 * It should be completely removed for release.
 *
 * Setting useImageLogging will disable it.
 */
public class localLogger {

    public static boolean useImageLogging = true; // Whether to save captured images for debugging.

    private static final String TAG = "localLogger";
    private static String basePath = "OxfordSDKImageLogging";
    private static final Boolean useJPEG = false;
    private static String lastSavedImageFileName = "";


    public static void saveImage(ImageHolder imageHolder, String extraData) {
        if (!useImageLogging) {return;}
        String pathAndFilename = getImageFilepath(extraData);
        byte[] imageData = imageHolder.data;
        Bitmap bmp = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        File pictureFile = new File(pathAndFilename);
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            if (useJPEG) {
                bmp.compress(Bitmap.CompressFormat.JPEG, 95, fos);
            } else {
                bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            }
            fos.close();
            lastSavedImageFileName = pathAndFilename;
            Log.d(TAG, "File" + pathAndFilename + " saved");
        } catch (Exception error) {
            Log.d(TAG, "File" + pathAndFilename + " not saved: " + error.getMessage());
            return;
        }
        return;
    }

    private static String getImageFilepath(String extraData) {
        String fileExtension = useJPEG ? ".jpg" : ".png";
        String fileName = String.format("Snap_%s_%d%s", extraData, System.currentTimeMillis(), fileExtension);
        return getDirectoryName(Environment.DIRECTORY_PICTURES) + fileName;
    }

    private static String getDirectoryName(String standardDirectory) {
        File dirFile  = new File(Environment.getExternalStoragePublicDirectory(standardDirectory),basePath);
        if (!dirFile.exists()) {
            try {
                if (!dirFile.mkdirs()) {
                    Log.d(TAG, "Can't create directory for logging.");
                }
            } catch ( SecurityException e ){
                Log.d(TAG, "Security exception creating directory for logging: " + e.getMessage() );
            }
        }
        return dirFile.getPath() + File.separator;
    }



    public static void logInfoAboutLastSample(String requestBy, String processingResult) {

        if (!useImageLogging) {return;}
        String fileExtension = ".txt";
        String fileName = "Snap_Metadata";
        String pathAndFilename = getDirectoryName(Environment.DIRECTORY_PICTURES) + fileName + fileExtension;
        String logResult = String.format("'%s', '%s', '%s'%n", lastSavedImageFileName, requestBy, processingResult) ;
        Log.d(TAG, "Logging text to file: " + logResult);
        try {
            FileWriter fw = new FileWriter(pathAndFilename, true);
            fw.write(logResult);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            Log.d(TAG, "IO exception writing to log file: " + e.getMessage() );
        }

    }
}
